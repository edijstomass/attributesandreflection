﻿using System;
using System.Reflection;

namespace ReflectionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Cola C = new Cola();
            Console.WriteLine(C.GetType()); // sii ir objekta iebuvētā metode
            //Type T2 = C.GetType(); // sadi ari var 
            Console.WriteLine();
            
            //Type T = Type.GetType("ReflectionExample.Cola");  long version
            Type T = typeof(Cola);  // short version
            Console.WriteLine(T.FullName); // prints types full name
            Console.WriteLine(T.Name); // only class (type) name
            Console.WriteLine(T.IsClass); // bool check if this type is a class.. returns true or false
            Console.WriteLine(T.GetProperties()); // can't access, becouse he is returning PropertyInfo[]
            Console.WriteLine();

           PropertyInfo[] properties = T.GetProperties();
            foreach (var prop in properties)
            {
                Console.WriteLine("Property name: " + prop.Name + ",  Property type: " + prop.PropertyType);
            }
            Console.WriteLine();

            MethodInfo[] methods = T.GetMethods();
            foreach (var meth in methods)
            {
                Console.WriteLine("Method name: " + meth.Name + ",  Is method public: " + meth.IsPublic + ", Return Type: "+ meth.ReturnType.Name);
            }
            Console.WriteLine();

        }
    }
}
