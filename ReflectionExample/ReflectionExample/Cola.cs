﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReflectionExample
{
    class Cola
    {

        public string name { get; set; }
        public int id { get; set; }
        

        public Cola()
        {
            this.name = "No Name";
            this.id = -1;
        }


        public void PrintName()
        {
            Console.WriteLine(this.name);
        }

        public void PrintId()
        {
            Console.WriteLine(this.id);
        }

    }
}
