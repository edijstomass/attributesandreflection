﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace WindowsFormsAppExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Button1_Click(object sender, EventArgs e)
        {
            String TypeName = textBox1.Text;
            Type T = Type.GetType(TypeName);

            lstMethods.Items.Clear();
            lstProperties.Items.Clear();
            lstConstructors.Items.Clear();

            MethodInfo[] methods = T.GetMethods();
            foreach (MethodInfo meth in methods)
            {
                lstMethods.Items.Add(meth.Name);
            }

            PropertyInfo[] properties = T.GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                lstProperties.Items.Add(prop.Name);
            }

            ConstructorInfo[] construcotrs = T.GetConstructors();
            foreach (ConstructorInfo ctor in construcotrs)
            {
                lstConstructors.Items.Add(ctor.ToString());
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
