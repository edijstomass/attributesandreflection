﻿using System;

namespace AttributeExample
{
    class Program
    {
        static void Main(string[] args)
        {

            Add(5, 10); 


            int[] numbers = new int[] { 5, 5, 5, 5 };
            Add(numbers);

            Add(new int[] { 12, 11, 15, 1, 2, 44 }); // or in one line
            





        }

        [Obsolete("This method is old, please use Add(int[] numbers")]   // this is attribute Obsolete
        public static int Add(int num1, int num2)
        {
            int sum = num1 + num2;
            Console.WriteLine($"sum is: {sum}");
            return sum;
        }


        public static int Add(int[] numbers)
        {
            int sum = 0;
            foreach (var number in numbers)
            {
                sum += number;
            }
            Console.WriteLine($"sum is: {sum}");
            return sum;
        }
    }



}


