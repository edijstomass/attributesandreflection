﻿using System;
using System.Reflection;

namespace LateBindingUsingReflections
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "This is early binding"
            //Customer C1 = new Customer();
            //string fullName = C1.PrintFullName("Edijs", "Tomass");
            //Console.WriteLine(fullName);
            #endregion

            #region"This is late binding. Use only when working with objects that are not available at compile time"
            //creates object
            Assembly executingAssembly = Assembly.GetExecutingAssembly(); // izveido assembly
            Type customerType = executingAssembly.GetType("LateBindingUsingReflections.Customer");  // izveido type
            object customerInstance = Activator.CreateInstance(customerType);   // izveido type instance
           
            //creates method
            MethodInfo printFullNameMethod = customerType.GetMethod("PrintFullName"); // izveido type member(method)

            //creates parameters
            string[] parameters = new string[2];
            parameters[0] = "name";
            parameters[1] = "lastname";
            
            //runs the method
            string fullName = (string)printFullNameMethod.Invoke(customerInstance, parameters);
            Console.WriteLine(fullName);
            #endregion

        }
    }


    class Customer
    {
        public string PrintFullName(string name, string lastname)
        {
            return name + " " + lastname;
        }
    }


}
